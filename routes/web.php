<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\welcomeController;
use App\Http\Controllers\providersController;
use App\Http\Controllers\dashboardController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [ welcomeController::class,'index']);

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', [ dashboardController::class,'index' ])->name('dashboard');
    Route::post('/new_providers', [ providersController::class,'register']);
    Route::get('/get_providers',[ providersController::class, 'show']);
    Route::post('/add_provider',[ providersController::class, 'add']);
    Route::post('/delete_provider',[ providersController::class, 'delete']);
});
