<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class provider extends Model
{
    use HasFactory;
    
    public function scopeGetProvider($query,$id){
        $provider =  $query->where("user_id","=",$id)->get()[0];
        return $provider;
    }
}
