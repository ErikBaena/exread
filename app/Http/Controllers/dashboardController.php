<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;
use Illuminate\Support\Facades\Auth;
use App\Models\provider;

class dashboardController extends Controller
{
    public function index() {
        $view = "AdminView";
        $user = Auth::user();
        $id = $user['id'];
        
        $data = [
            "user" =>$user
        ];
        if($user->type == 2){
            $view = "ProviderView";
            $provider = provider::GetProvider($id);
            $data['provider'] = $provider;
        }
        return Inertia::render($view,$data);
    }
}
