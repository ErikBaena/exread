<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\provider;
use App\Models\user;

class providersController extends Controller
{
    public function register(Request $rquest){
        $file = $rquest->file('file-0');
        $openfile = fopen($file, "r");
        $content = fread($openfile, filesize($file));
        $providers = explode(";",$content);
        $providers_array = [];
        $counter = 0;
        foreach ($providers as $provider_str) {
            if($provider_str != ""){
                $provider = explode(",",$provider_str);
                foreach ($provider as $value_str) {
                    if($value_str != ""){
                        $value = explode(":",$value_str);
                        $providers_array[$counter][strtolower($value[0])] = $value[1];
                    }
                }
            }
            $counter ++;
        }
        $providers_registred = provider::all();
        $providers_return = [];
        
        foreach ($providers_array as $provider_array) {
            $counter = 0;
            foreach ($providers_registred as $provider_registred) {
                if($provider_array['rfc'] == $provider_registred['rfc']){
                    $counter ++;
                }
            }
            if($counter == 0){
                $providers_return[] = $provider_array;
            }
        }

        return $providers_return;
    }

    public function show(){
        return provider::all();
    }
    public function add(Request $request){

        $user = new user();
        $user->name = $request->name;
        $user->password = Hash::make($request->email);
        $user->email = $request->email;
        $user->type = 2;
        $user->save();
        $id = $user->id;

        $provider = new provider();
        $provider->name = $request->name;
        $provider->rfc = $request->rfc;
        $provider->email = $request->email;
        $provider->user_id = $id;
        $provider->save();
       
        return [];
    }

    public function delete(Request $request){
        $id = $request->id;
        $provider = provider::find($id);
        $user_id = $provider->user_id;
        $user = user::find($user_id);
        $provider->delete();
        $user->delete();
        return [];
    }
}
